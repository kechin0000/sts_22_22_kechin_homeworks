public class Main {
    public static void main(String[] args) {

        int[] array1 = {3, 7, 9, 14, 25, 30, 33, 48, 54, 66};

        // первое лямбда выражение
        ArrayTask Sum = (array, from, to) -> {
            int sum = 0;
            for (int i = from; i <= to; i++) {
                sum = sum + array[i];
            }
            System.out.println("Cумма элементов массива в промежутке от "
                    + from + " до " + to + " равна " + sum);
            return sum;
        };

        // второе лямбда выражение
        ArrayTask Sum2 = (array, from, to) -> {
            int max = array1[0];
            for (int i = from; i < to; i++)
                max = Math.max(array1[from], array1[to]);
            System.out.println("Максимальное число в промежутке от " + from + " до " + to + " = " + max);
            int sum = 0;
            int maxSum = 0;

            sum = sum + max % 10;
            maxSum = sum + max / 10;

            System.out.println("Сумма цифр максимального числа в промежутке от " + from + " до " + to + " = " + maxSum);
            return max;
        };
        ArraysTasksResolver.resolveTask(array1, Sum, 1, 6);
        ArraysTasksResolver.resolveTask(array1, Sum2, 1, 6);
    }
}