import java.util.Arrays;

public class ArraysTasksResolver {
    public static void resolveTask(int[] array, ArrayTask task, int from, int to) {
        System.out.println(Arrays.toString(array));
        System.out.println("from: " + from);
        System.out.println("to: " + to);
        task.resolve(array, from, to);
    }

}
