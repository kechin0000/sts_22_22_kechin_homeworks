import java.io.*;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ProductsRepositoryFileBasedImpl implements ProductsRepository {

    private final String fileName;

    public ProductsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<String, Product> stringToProductMapper = currentProduct -> {
        String[] parts = currentProduct.split("\\|");
        int id = Integer.parseInt(parts[0]);
        String name = parts[1];
        Double cost = Double.parseDouble(parts[2]);
        int count = Integer.parseInt(parts[3]);

        return new Product(id, name, cost, count);
    };

    private static final Function<Product, String> productToStringMapper = product ->
            product.getId() + "|" + product.getName() + "|" + product.getCost().toString() + "|" + product.getCount();

    @Override
    public List<Product> findAll() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToProductMapper)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public Product findById(Integer id) {
        try {
            return (Product) new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToProductMapper)
                    .filter(p -> id.equals(p.getId()))
                    .toArray()[0];
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public List<Product> findAllByTitleLike(String title) {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToProductMapper)
                    .filter(p -> !p.getName().isEmpty() && p.getName().contains(title))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    public int finIdByIdField(int id, List<Product> list) {
        for (int i = 0; i <= list.size(); i++) {
            if (list.get(i).getId() == id) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void update(Product product) {
        List<Product> productList = findAll();

        productList.set(finIdByIdField(product.getId(), productList), product);

        String productToString = productList.stream().map(productToStringMapper)
                .map((s) -> s + "\r\n").collect(Collectors.joining());

        try (FileWriter fileWriter = new FileWriter(fileName, false);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            bufferedWriter.write(productToString);
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }
}

