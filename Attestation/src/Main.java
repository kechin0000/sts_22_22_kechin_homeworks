import java.util.List;

public class Main {
    public static void main(String[] args) {
        ProductsRepository usersRepository = new ProductsRepositoryFileBasedImpl("Product.txt");

        System.out.println(usersRepository.findAllByTitleLike("о"));
        System.out.println(usersRepository.findById(3));

        List<Product> ProductList = usersRepository.findAll();
        Product product = usersRepository.findById(3);

        product.setCount(33);
        product.setCost(3.);

        usersRepository.update(product);
    }
}
