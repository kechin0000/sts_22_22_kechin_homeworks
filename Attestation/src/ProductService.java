public class ProductService {

    private ProductsRepository productsRepository;

    public ProductService(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }

    public int countAllProducts() {
        return productsRepository.findAll().size();
    }
}
