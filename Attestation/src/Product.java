public class Product {

    private int id;
    private String name;
    private Double cost;
    private int count;

    public Product(int id, String name, Double cost, int count) {
        this.id = id;
        this.name = name;
        this.cost = cost;
        this.count = count;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Double getCost() {
        return cost;
    }

    public int getCount() {
        return count;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return name + "|" + cost + "|" + count;
    }
}
