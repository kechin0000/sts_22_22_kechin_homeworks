package ru.inno.ec.models;

import javax.persistence.*;
import lombok.*;

import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@EqualsAndHashCode(exclude = "course")
@ToString(exclude = "course")
public class Lesson {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    @Column(length = 1000)
    private String summary;

    @Column(name = "start_sale")
    private LocalTime startTime;

    @Column(name = "finish_sale")
    private LocalTime finishTime;

    @ManyToOne
    @JoinColumn(name = "buy_id")
    private Course course;
}
