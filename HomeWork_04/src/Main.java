public class Main {
    public static int calcSumOfArrayRange(int[] a, int from, int to) {
        int sum = 0;
        int error = -1;
        if (from >= to) {
            return error;
        } else {
            for (int i = from; i <= to; i++) {
                sum += a[i];
            }
        }
        return sum;
    }

    public static void printEvenNum(int array[]) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0) {
                System.out.println(array[i]);
            }
        }
    }

    public static void main(String[] args) {
        int[] array = {11, 22, 33, 44, 55, 66, 77, 88, 99, 110, 111, 112, 113, 114, 115};
        int sum = calcSumOfArrayRange(array, 3, 9);
        int sum2 = calcSumOfArrayRange(array, 11, 9);

        System.out.println(sum + " " + sum2);
        printEvenNum(array);
    }
}
