package ru.inno;

import ru.inno.education.models.Car;
import ru.inno.education.repostories.CarRepositoriesJdbcImpl;
import ru.inno.education.repostories.CarRepository;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

public class Main {
    public static void main(String[] args) {
        Properties dbProperties  = new Properties();
        try {
            dbProperties.load(new BufferedReader(
                    new InputStreamReader(Main.class.getResourceAsStream("/db.properties"))));

            Connection connection = DriverManager.getConnection(
                    dbProperties.getProperty("db.url"),
                    dbProperties.getProperty("db.username"),
                    dbProperties.getProperty("db.password"));

            CarRepository carRepository = new CarRepositoriesJdbcImpl(connection);
            List<Car> cars = carRepository.findAll();
            System.out.println(cars);
        } catch (IOException | SQLException e){
            throw new IllegalArgumentException(e);
        }
    }
}