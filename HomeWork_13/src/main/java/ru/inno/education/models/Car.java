package ru.inno.education.models;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Car {
    private  long id;
    private String model;
    private String color;
    private String  number;
    private long owner_id;
}
