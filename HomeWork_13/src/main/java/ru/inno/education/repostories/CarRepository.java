package ru.inno.education.repostories;

import ru.inno.education.models.Car;

import java.util.List;

public interface CarRepository {
   List<Car> findAll();
}
