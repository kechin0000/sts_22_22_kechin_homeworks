package ru.inno.education.repostories;

import ru.inno.education.models.Car;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CarRepositoriesjdbcImpl implements CarRepository {

    //Language=SQL
    private static final String SQL_SELECT_ALL = "select * from car order by id";
    private Connection connection;

    public CarRepositoriesjdbcImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<Car> findAll() {
        List<Car> cars = new ArrayList<>();
        try (Statement statement = connection.createStatement()){
           try(ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL)) {
              while (resultSet.next()){
                  Car car = Car.builder()
                          .id(resultSet.getLong("id"))
                          .model(resultSet.getString("model"))
                          .color(resultSet.getString("color"))
                          .number(resultSet.getString("number"))
                          .owner_id(resultSet.getLong("owner_id"))
                          .build();
                  cars.add(car);

              }
           }

        }catch (SQLException e){
            throw new IllegalStateException(e);
        }
        return cars ;
    }
}
