public class circle extends figure {
    private int r;

    public circle(int figurecenterX, int figurecenterY, int r) {
        super(figurecenterX, figurecenterY);
        this.r = r;
    }

    double p = 0;

    public void perimeter(int r) {
        p = 2 * 3.1415926535 * r;
        System.out.println("Периметр круга = " + p);
    }

    double s = 0;

    public void square(int r) {
        s = 3.1415926535 * (r * r);
        System.out.println("Площадь круга = " + s);
    }

    public int getR() {
        return r;
    }

    public void setR(int r) {
        this.r = r;
    }
}
