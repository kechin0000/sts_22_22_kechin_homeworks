public class quadrate extends figure {
    private int a;

    public quadrate(int figurecenterX, int figurecenterY, int a) {
        super(figurecenterX, figurecenterY);
        this.a = a;
    }

    public void perimeter(int a) {
        a *= 4;
        System.out.println("Периметр квадрата = " + a);
    }

    public void square(int a) {
        a *= a;
        System.out.println("Площадь квадрата = " + a);
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }
}
