import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        quadrate q = new quadrate(0, 0, 0);
        q.setFigurecenterX(20);
        q.setFigurecenterY(25);
        q.setA(6);
        q.position(q.getFigurecenterX(), q.getFigurecenterY());
        q.perimeter(q.getA());
        q.square(q.getA());
        q.move(1, 2);

        rectangle r = new rectangle(0, 0, 0, 0);
        r.setFigurecenterX(40);
        r.setFigurecenterY(-50);
        r.setA(6);
        r.setB(7);
        r.position(r.getFigurecenterX(), r.getFigurecenterY());
        r.perimeter(r.getA(), r.getB());
        r.square(r.getA(), r.getB());
        r.move(1, 2);

        circle c = new circle(0, 0, 0);
        c.setFigurecenterX(4);
        c.setFigurecenterY(5);
        c.setR(6);
        c.position(c.getFigurecenterX(), c.getFigurecenterY());
        c.perimeter(c.getR());
        c.square(c.getR());
        c.move(1, 2);

        ellipse e = new ellipse(0, 0, 0,0);
        e.setFigurecenterX(4);
        e.setFigurecenterY(5);
        e.setR(6);
        e.setR2(7);
        e.position(e.getFigurecenterX(), e.getFigurecenterY());
        e.perimeter(e.getR(),e.getR2());
        e.square(e.getR(),e.getR2());
        e.move(1, 2);

    }
}