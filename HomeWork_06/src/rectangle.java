public class rectangle extends quadrate{
    private int b;
    public rectangle (int figurecenterX, int figurecenterY, int a,int b){
        super(figurecenterX, figurecenterY, a);
        this.b = b;
    }
    int p = 0;
    public void perimeter(int a, int b) {
        p = (a+b)*2;
        System.out.println("Периметр прямоугольника = " + p);
    }
    int s = 0;
    public void square(int a, int b) {
        s = a*b;
        System.out.println("Площадь прямоугольника = " + s);
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }
}
