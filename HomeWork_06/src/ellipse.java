public class ellipse extends circle {
    private int r2;

    public ellipse(int figurecenterX, int figurecenterY, int r, int r2) {
        super(figurecenterX, figurecenterY, r);
        this.r2 = r2;
    }

    double p = 0;

    public void perimeter(int r, int r2) {
        p = 4 * ((3.1415926535 * r * r2) + (r - r2) * (r - r2) * (r - r2) / (r + r2));
        System.out.println("Периметр эллипса = " + p);
    }

    double s = 0;

    public void square(int r, int r2) {
        s = 3.1415926535 * r * r2;
        System.out.println("Площадь эллипса = " + s);
    }

    public int getR2() {
        return r2;
    }

    public void setR2(int r2) {
        this.r2 = r2;
    }
}
