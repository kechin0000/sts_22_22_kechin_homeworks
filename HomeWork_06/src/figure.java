public class figure {
    private int figurecenterX;
    private int figurecenterY;

    public figure(int figurecenterX, int figurecenterY) {
        this.figurecenterX = figurecenterX;
        this.figurecenterY = figurecenterY;
    }

    public void position(int x, int y) {
        System.out.println("Фигура в координатах(" + x + ";" + y + ")");
    }
    public void move(int toX, int toY) {
        System.out.println("Фигура перемещена в координаты(" + toX + ";" + toY + ")\n");
    }

    public int getFigurecenterX() {
        return figurecenterX;
    }

    public int getFigurecenterY() {
        return figurecenterY;
    }

    public void setFigurecenterX(int figurecenterX) {
        this.figurecenterX = figurecenterX;
    }

    public void setFigurecenterY(int figurecenterY) {
        this.figurecenterY = figurecenterY;
    }
}
