import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размер массива:");
        int length = scanner.nextInt();
        int[] array = new int[length];
        int i = 0;
        System.out.println("Заполните массив:");
        for (i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        System.out.println(Arrays.toString(array));
        int localmin = 0;
        for (i = 0; i < array.length; i++) {
            if (i == 0) {
                if (array[i] < array[i + 1]) {
                    localmin++;
                }
            } else if (i == array.length - 1) {
                if (array[i] < array[i - 1]) {
                    localmin++;
                }
            } else {
                if ((array[i] < array[i - 1] && array[i] < array[i + 1])) {
                    localmin++;
                }
            }
        }
        System.out.println("Локальных минимумов в массиве: " + localmin);
    }
}