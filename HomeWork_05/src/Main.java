import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        atm one = new atm(900, 100, 1000, 0);
        Scanner scanner = new Scanner(System.in);
        System.out.println("В банкомате: " + one.cash);
        System.out.println("Лимит средств: " + one.maxCash);
        System.out.println("Максимальная сумма, разрешенная к выдаче: " + one.maxDelivery);
        System.out.println("Какую сумму вы хотите снять?");
        int giveCash = scanner.nextInt();
        one.giveMoney(giveCash);
        System.out.println("Какую сумму вы хотите внести?");
        int takeCash = scanner.nextInt();
        one.takeMoney(takeCash);
    }
}
