
public class atm {
    int cash;
    public final static int maxDelivery = 100;
    public final static int maxCash = 1000;
    int operations;

    atm(int cash, int maxDelivery, int maxCash, int operations) {
        this.cash = cash;
        this.operations = operations;
    }

    void giveMoney(int money) {
        if (money <= cash && money <= maxCash && money < maxDelivery) {
            cash -= money;
            operations++;
            System.out.println("Выдано: " + money);
            System.out.println("Денег в банкомате осталось: " + cash);
            System.out.println("Операций выполнено: " + operations);
        } else {
            System.out.println("error");

        }
    }

    void takeMoney(int money) {
        if (money + cash < maxCash) {
            cash += money;
            operations++;
            System.out.println("Внесено: " + money);
            System.out.println("Денег в банкомате осталось: " + cash);
            System.out.println("Операций выполнено: " + operations);
        } else {
            int surrender = cash + money - maxCash;
            cash = cash + money - surrender;
            operations++;
            System.out.println("Внесено: " + (money - surrender));
            System.out.println("Ваша сдача: " + surrender);
            System.out.println("Денег в банкомате осталось: " + cash);
            System.out.println("Операций выполнено: " + operations);
        }
    }
}
