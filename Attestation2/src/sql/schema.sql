drop table if exists car cascade;
drop table if exists driver cascade;
drop table if exists ride cascade;

create table user
(
    id               serial primary key,
    first_name       char(20)                                              default 'DEFAULT_NAME',
    last_name        char(20)                                              default 'DEFAULT_LAST_NAME',
    phone_number     char(12)                                              default 0,
    experience       integer check (experience >= 0 and experience <= 60 ) default 0,
    age              integer check (age >= 20 and age <= 81) not null,
    have_license     bool                                    not null,
    license_category varchar(5)                                            default 'B',
    rating           integer check (rating >= 0 and rating <= 5)           default 0
);

create table car
(
    id       serial primary key,
    model    char(20)       default 'Subaru Outback',
    color    char(20) default 'COLOR',
    number   char(9) unique not null,
    owner_id integer        not null,
    foreign key (owner_id) references driver (id)
);

create table ride
(
    driver_id     integer not null,
    car_id        integer not null,
    ride_date     timestamp,
    ride_duration time,
    foreign key (driver_id) references driver (id),
    foreign key (car_id) references car (id)
)










insert into driver (age, have_license)
values (21, true),
       (22, false),
       (21, true),
       (22, false),
       (21, true);

insert into car (number, owner_id)
values ('х336ак056', 3),
       ('х366ак056', 2),
       ('х396ак056', 1),
       ('х696ак056', 4),
       ('х996ак056', 5);


insert into ride (driver_id, car_id, ride_date, ride_duration)
values (5, 1, '2011-11-11', '13:00'),
       (1, 3, '2011-11-11', '14:00'),
       (2, 4, '2011-11-11', '15:00'),
       (3, 4, '2011-11-11', '16:00'),
       (4, 5, '2011-11-11', '17:00');