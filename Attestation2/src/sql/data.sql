


drop table if exists users cascade;
drop table if exists score cascade;
drop table if exists bye cascade;

create table users
(
    id         serial primary key,
    email      varchar(50)                             not null,
    password   integer                                 not null,
    first_name char(20)                                not null,
    last_name  char(20)                                not null,
    age        integer check (age >= 20 and age <= 81) not null

);
create table score
(
    id         serial primary key,
    name       char(40) not null,
    start_date timestamp,
    final_date timestamp,
    user_id    integer  not null,
    foreign key (user_id) references users (id)
);

create table bye
(
    id         serial primary key,
    product    char(40) not null,
    event_date timestamp,
    user_id    integer  not null,
    foreign key (user_id) references users (id)
);

insert into users (email, password, first_name, last_name, age)
values ("qwerty1@ya.ru", 1111, "f_name1", "l_name1", 20),
       ("qwerty2@ya.ru", 1111, "f_name2", "l_name2", 22),
       ("qwerty3@ya.ru", 1111, "f_name3", "l_name3", 24);


insert into score (number, owner_id)
values ('х336ак056', 3),
       ('х366ак056', 2),
       ('х396ак056', 1),
       ('х696ак056', 4),
       ('х996ак056', 5);


insert into bye (driver_id, car_id, ride_date, ride_duration)
values (5, 1, '2011-11-11', '13:00'),
       (1, 3, '2011-11-11', '14:00'),
       (2, 4, '2011-11-11', '15:00'),
       (3, 4, '2011-11-11', '16:00'),
       (4, 5, '2011-11-11', '17:00');
