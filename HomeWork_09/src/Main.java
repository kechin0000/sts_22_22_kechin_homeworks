import java.util.ListIterator;
public class Main {

    public static String mergeDocuments(Iterable<String> documents) {
        StringBuilder mergedDocument = new StringBuilder();

        Iterator<String> documentsIterator = documents.iterator();

        while (documentsIterator.hasNext()) {
            mergedDocument.append(documentsIterator.next() + " ");
        }

        return mergedDocument.toString();
    }

    public static void main(String[] args) {
        List<Integer> intList = new ArrayList<>();

        intList.add(33);
        intList.add(2);
        intList.add(12);
        intList.add(7);
        intList.add(54);
        intList.add(-6);
        intList.add(-65);
        intList.add(45);
        intList.add(5);
        intList.add(5);
        intList.add(13);

        intList.remove(-6);
        intList.remove(45);

        intList.removeAt(1);
        intList.removeAt(10);

        System.out.println(intList.get(0));
        System.out.println(intList.get(1));
        System.out.println(intList.get(2));
        System.out.println(intList.get(3));
        System.out.println(intList.get(4));
        System.out.println(intList.get(5));
        System.out.println(intList.get(6));
        System.out.println(intList.get(7));
        System.out.println(intList.get(8));
        System.out.println(intList.get(9));
        System.out.println(intList.get(10));




    }
}