public class Main {
    public static void main(String[] args) {
        CarRepository carRepository = new CarRepositoryFileBasedImpl("car.txt");

        Car lada = new Car("x333ак", "lada", "white", 200, 40_000);

        carRepository.save(lada);

        System.out.println(carRepository.findAll() + "\n");
        System.out.println(carRepository.findNumberByColorOrMileage("Black", 0) + "\n");
        System.out.println(carRepository.findAllUniqueCarInRangeOfPrice(70_000, 85_000) + "\n");
        System.out.println(carRepository.findColorByMinimalPrice() + "\n");
        System.out.println(carRepository.findAveragePriceOfCar("Camry") + "\n");
    }
}