abstract class AbstractNumbersPrintTask implements Task {
    protected int from;
    protected int to;

    AbstractNumbersPrintTask(int from, int to) {
        this.from = from;
        this.to = to;
    }

    public int getFrom() {
        return from;
    }

    public int getTo() {
        return to;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public void setTo(int to) {
        this.to = to;
    }
}
