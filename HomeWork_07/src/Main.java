public class Main {
    static void completeAllTasks(Task[] tasks) {
        for (int i = 0; i < tasks.length; i++) {
            tasks[i].complete();
        }
    }

    public static void main(String[] args) {
        AbstractNumbersPrintTask e = new EvenNumbersPrintTask(0, 50);
        AbstractNumbersPrintTask o = new OddNumbersPrintTask(50, 100);

        Task[] tasks = {e, o};

        completeAllTasks(tasks);
    }
}

