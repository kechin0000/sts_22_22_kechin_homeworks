import java.sql.Array;

public class EvenNumbersPrintTask extends AbstractNumbersPrintTask implements Task {
    EvenNumbersPrintTask(int from, int to) {
        super(from, to);
    }

    public void complete() {
        System.out.println("Четные числа в диапазоне " + from + " - " + to);
        for (int i = from; i < to + 1; i++) {
            if (i % 2 == 0) {
                System.out.println(i);
            }
        }
    }
}

