public class OddNumbersPrintTask extends AbstractNumbersPrintTask implements Task {
    OddNumbersPrintTask(int from, int to) {
        super(from, to);
    }

    public void complete() {
        System.out.println("Нечетные числа в диапазоне " + from + " - " + to);
        for (int i = from; i < to + 1; i++) {
            if (i % 2 == 1) {
                System.out.println(i);
            }
        }
    }

}
