insert into student (email, password, age, is_worker)
values ('kechin0000@gmail.com', '32z12100', 22, false),
       ('mike_miami@mail.ru', '121z121', 22, true),
       ('vadimermak@ya.ru', 'na_three', 22, true),
       ('nikolalab@gmail.com', 'b4g6Nk', 22, false)

update student
set first_name = 'Bogdan',
    last_name  = 'Kechin'
where id = 1;
update student
set first_name = 'Michail',
    last_name  = 'Semikov'
where id = 2;
update student
set first_name = 'Vadim',
    last_name  = 'Ermakov'
where id = 3;
update student
set first_name = 'Nikolay',
    last_name  = 'Hodokov'
where id = 4;

insert into course (title, description, start, finish)
values ('Java', 'Разработка на Java', '2022-11-07', '2023-02-02'),
       ('PHP', 'Разработка на PHP', '2022-10-01', '2024-02-02'),
       ('SQL', 'Введение в работу с БД', '2022-09-01', '2025-02-02'),
       ('Spring', 'Разработка на Java с использованием Spring', '2020-01-01', '2022-01-01')
    insert
into lesson (name, summary, start_time, finish_time, course_id)
values ('ООП', 'Объекты и классы', '09:00', '12:00', 1), ('Почему PHP', 'Потому что', '09:00', '12:00', 2), ('Индексы в БД', 'Есть btree', '09:00', '12:00', 3), ('Бины', 'Бины в спринг', '09:00', '12:00', 4);

insert into lesson (name, summary, start_time, finish_time, course_id)
values ('бд', 'бдбд', '09:00', '12:00', 3);

insert into student_course(student_id, course_id)
values (1, 1),
       (1, 2),
       (2, 3),
       (4, 3),
       (1, 3),
       (2, 4)